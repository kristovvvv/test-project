import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, SQLException {
        DataHandler dataHandler = new DataHandler();
        dataHandler.setup();
        dataHandler.readXMLdata("vf:CastObce");
        dataHandler.readXMLdata("vf:Obec");
        dataHandler.sqlInsertion();
    }

}

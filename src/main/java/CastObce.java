public class CastObce {
    private int kod;
    private String nazev;
    private int kodObce;

    @Override
    public String toString() {
        return "CastObce{" +
                "kod=" + kod +
                ", nazev='" + nazev + '\'' +
                ", kodObce=" + kodObce +
                '}';
    }

    public CastObce(int kod, int kodObce, String nazev) {
        this.kod = kod;
        this.nazev = nazev;
        this.kodObce = kodObce;
    }

    public String sqlInsert() {
        return "insert into castobce (kod, kodobce, nazev) values ("+getKod()+", "+getKodObce()+", '"+getNazev()+"');";
    }

    public int getKod() {
        return kod;
    }


    public String getNazev() {
        return nazev;
    }


    public int getKodObce() {
        return kodObce;
    }
}

import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataHandler {

    private String kod;
    private String nazev;
    private int kodObce;
    final static String urlSource = "https://vdp.cuzk.cz/vymenny_format/soucasna/20210430_OB_573060_UZSZ.xml.zip";
    final static Path downloadLocation = Paths.get(System.getProperty("user.home"), "Downloads\\20210430_OB_573060_UZSZ.xml.zip");
    final static Path extractedFileLocation = Paths.get(System.getProperty("user.home"), "Downloads");

    private ArrayList<CastObce> castObceList;
    private ArrayList<Obec> obecList;

    public DataHandler() {
        this.castObceList = new ArrayList<>();
        this.obecList = new ArrayList<>();
    }

    // downloads .zip file from url to basic download location
    // extracts .zip file
    public void setup() throws IOException {
        FileUtils.copyURLToFile(new URL(urlSource), new File(downloadLocation.toString()));
        ZipFile zipFile = new ZipFile(downloadLocation.toString());
        zipFile.extractAll(extractedFileLocation.toString());
    }


    // reads wanted data from xml file
    public void readXMLdata(String xmlElement) throws ParserConfigurationException, IOException, SAXException {
        File file = new File(extractedFileLocation+"\\20210430_OB_573060_UZSZ.xml");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(file);
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName(xmlElement);
        for (int itr = 0; itr < nodeList.getLength(); itr++)
        {
            Node node = nodeList.item(itr);
            if (node.getNodeType() == Node.ELEMENT_NODE)
            {
                switch (xmlElement) {
                    case "vf:CastObce":
                        Element eElement = (Element) node;
                        kod = eElement.getElementsByTagName("coi:Kod").item(0).getTextContent();
                        nazev = eElement.getElementsByTagName("coi:Nazev").item(0).getTextContent();
                        kodObce = Integer.parseInt(eElement.getElementsByTagName("obi:Kod").item(0).getTextContent());
                        castObceList.add(new CastObce(Integer.parseInt(kod), kodObce, nazev));
                        break;
                    case "vf:Obec":
                        eElement = (Element) node;
                        kod = eElement.getElementsByTagName("obi:Kod").item(0).getTextContent();
                        nazev = eElement.getElementsByTagName("obi:Nazev").item(0).getTextContent();
                        obecList.add(new Obec(Integer.parseInt(kod), nazev));
                        break;
                }
            }
        }
    }

    // inserts data into database
    public void sqlInsertion() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String username = "postgres";
        String password = "Bdnhg123";
        deleteOldDbsData();
        Connection conn = DriverManager.getConnection(url,username, password);
        Statement st = conn.createStatement();
        for (Obec o: obecList
             ) {
            System.out.println(o.sqlInsert());
            st.execute(o.sqlInsert());
        }
        for (CastObce c: castObceList
             ) {
            System.out.println(c.sqlInsert());
            st.execute(c.sqlInsert());
        }
    }

    // deletes old data from dbs
    public void deleteOldDbsData() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String username = "postgres";
        String password = "Bdnhg123";
        Connection conn = DriverManager.getConnection(url,username, password);
        Statement st = conn.createStatement();
        st.execute("delete from castobce where kodobce = 573060;\n" +
                "delete from obec where kod = 573060;");
    }
}

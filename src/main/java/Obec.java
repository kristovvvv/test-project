public class Obec {
    private int kod;
    private String nazev;

    @Override
    public String toString() {
        return "Obec{" +
                "kod=" + kod +
                ", nazev='" + nazev + '\'' +
                '}';
    }

    public Obec(int kod, String nazev) {
        this.kod = kod;
        this.nazev = nazev;
    }

    public String sqlInsert() {
        return "insert into obec (kod, nazev) values("+getKod()+", '"+getNazev()+"');";
    }

    public int getKod() {
        return kod;
    }

    public String getNazev() {
        return nazev;
    }
}
